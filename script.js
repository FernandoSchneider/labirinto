const btnConfirm = document.getElementById('btnConfirm')
const btnCreate = document.getElementById('btnCreate');
const map = [
    "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW",
    "W   W     W       W           W",
    "W W W WWW W WWWWW W WWWWWWWWW W",
    "W W W   W   W     W       W   W",
    "W WWWWWWW W WWWWWWWWW WWW W WWW",
    "W         W       W   W   W   W",
    "W WWW W WWW WWWWW W WWW WWWWW W",
    "W W   W   W W W   W   W W W   W",
    "W WWWWWWW W W W W WWW W W W WWW",
    "S     W   W W W W W   W   W W F",
    "WWWWW W WWW W W W W WWWWWWW W W",
    "W     W   W   W W W   W   W W W",
    "W WWWWWWW WWWWW W W W WWW W W W",
    "W       W       W   W     W   W",
    "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW",
];

class neighbors{
    constructor (firstValue,secondValue) {
        this.ArrowLeft = `${firstValue} ${secondValue - 1}`;
        this.ArrowRight = `${firstValue} ${secondValue + 1}`;
        this.ArrowUp = `${firstValue - 1} ${secondValue}`;
        this.ArrowDown = `${firstValue + 1} ${secondValue}`;
    }
}

const toggleMessage = (value) => {
    const createLab = document.getElementById('create');
    const divWin = document.getElementById('win');

    divWin.classList.toggle('hidden');
    if (value) {
        createLab.classList.remove('hidden');
    }
}

const resetGame = () => {
    const sections = document.querySelectorAll('section');
    
    for (let i = 0; i < sections.length; i++) {

        let atual = sections[i];
        atual.remove()
    }
}

const winCondition = (atributeLocation) => {
    const winPosition = document.querySelector('.end').dataset.position;

    if (atributeLocation === winPosition) {
        toggleMessage();
        resetGame();
    }
}

const wallVerification = (location) => {
    let arrayClass = Array.from(location.classList);

    if (arrayClass.includes('wall')) {

        return true;
    } else {

        return false;
    }
}

const movePlayer = (direction) => {
    const player = document.getElementById('player');
    let allDivs = Array.from(document.querySelectorAll('.cell'));

    let actualPosition = player.parentElement.dataset.position.split(' ');
    let valueOne = parseInt(actualPosition[0]);
    let valueTwo = parseInt(actualPosition[1]);
    let playerNeighbors = new neighbors(valueOne, valueTwo);
    let atributeLocation = playerNeighbors[direction];

    let filterLocation = allDivs.filter(elem => {
        return elem.dataset.position === atributeLocation;
    });
    let newLocation = filterLocation[0];

    if (newLocation !== undefined && ! wallVerification(newLocation)) {

        newLocation.appendChild(player);
        winCondition(atributeLocation);
    }
}

const keyPress = (evt) => {
    let actualKey = evt.key;
    
    movePlayer(actualKey);
}

const createPlayer = () => {
    const player = document.createElement('img');
    const start = document.querySelector('.start');
    
    player.id = 'player';
    player.src = 'https://media.tenor.com/images/95c5349b410fcbd723e23f38a97e1d64/tenor.gif'
    start.appendChild(player);
}

const criarLab = () => {
    const lab = document.getElementById('lab');

    for (let i = 0; i < map.length; i++) {
        let linha = map[i];
        const createLinha = document.createElement('section');
        
        for (let j = 0; j < linha.length; j++) {
            let letter = linha[j];
            const div = document.createElement('div');
            div.classList.add('cell');
            div.dataset.position = `${i} ${j}`;

            if (letter === 'W') {
                div.classList.add('wall');
            } else if (letter === 'S') {
                div.classList.add('start');
            } else if (letter === 'F') {
                div.classList.add('end');
            } else {
                div.classList.add('path');
            }
    
            createLinha.appendChild(div);
        }
    
        lab.appendChild(createLinha);
    }
}

const init = () => {
    const createLab = document.getElementById('create');

    resetGame();
    criarLab();
    createPlayer();
    createLab.classList.add('hidden');
}

document.addEventListener('keydown', keyPress);
btnConfirm.addEventListener('click', function(){
    toggleMessage(true);
});
btnCreate.addEventListener('click', init);